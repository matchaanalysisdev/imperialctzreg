﻿using Imperial.Citizen.Registration.Data;
using Imperial.Citizen.Registration.Logs;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Imperial.Citizen.Registration
{
    public partial class Roles : System.Web.UI.Page
    {
        [Inject]
        public ICitizenRepository repo { get; set; }

        private void BindRolesList()
        {
            try
            {
                var roles = repo.GetRoles().OrderByDescending(r => r.Id).ToList();
                this.RoleList.DataSource = roles;
                this.RoleList.DataBind();
            }
            catch (Exception ex)
            {
                Logging.WriteLog("Role List", ex.Message);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindRolesList();
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            Data.Role newRole = new Role()
            {
                Created = DateTime.Now,
                RoleName = this.Name.Text
            };

            if (repo.AddRole(newRole))
            {
                repo.Save();
            }
            BindRolesList();
        }
    }
}