﻿"use strict";

var ImperialRegistration = (function () {
    var AuxClass = function () {
        //Set context
        var that = this;

        //Start of private properties

        //Set an object with the HTML objects we will use on the site
        var htmlElemnts = {
            _textInputs: null,
            textInputs: function () {
                if (this._textInputs == null) {
                    this._textInputs = $('.textInput');
                }
                return this._textInputs;
            },
            _citizenForm: null,
            citizenForm: function () {
                if (this._citizenForm == null) {
                    this._citizenForm = $('.citizenForm');
                }
                return this._citizenForm;
            },
            _citizenFormInputs: null,
            citizenFormInputs: function () {
                if (this._citizenFormInputs == null) {
                    this._citizenFormInputs = $('.citizenForm input:radio');
                }
                return this._citizenFormInputs;
            }

        };

        var blockUI = function () {
            var skyWalkerAlert = {
                "name": htmlElemnts.textInputs()[0].value,
                "specie": htmlElemnts.textInputs()[1].value,
                "date": Date.now()
            }
            htmlElemnts.citizenForm().attr('disabled', 'disabled');
            htmlElemnts.citizenFormInputs().attr('disabled', 'disabled');
            debugger;
            setCookie("skyWalkerAlert", skyWalkerAlert);
            alert("Skywalker detected UI blocked until darth vader comes back to life");
        };

        this.bindBlurEvent = function(){
            for (var i = 0; i < htmlElemnts.textInputs().length; i++) {
                $(htmlElemnts.textInputs()[i]).bind("change", function (event) {
                    if (this.value.toLowerCase() == "skywalker") {
                        blockUI();
                    }
                });
            }
        };

        var setCookie = function (key, object) {
            var value = JSON.stringify(object);
            var expires = new Date();
            expires.setTime(expires.getTime() + (value * 24 * 60 * 60 * 1000));
            document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
        };

        this.getCookie = function (key) {
            var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
            return keyValue ? keyValue[2] : null;
        };

    };
    return new AuxClass();
}());

//This autocalled fucntion will always be called on startup
(function (window, document, undefined) {
    window.onload = init;

    function init() {
        ImperialRegistration.bindBlurEvent();
    }

})(window, document, undefined);