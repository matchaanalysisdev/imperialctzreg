﻿"use strict";

var ImperialRebels = (function () {
    var AuxClass = function () {
        //Set context
        var that = this;

        //Start of private properties

        //Set an object with the HTML objects we will use on the site
        var htmlElemnts = {
            _rebelsList: null,
            rebelsList: function () {
                if (this._rebelsList == null) {
                    this._rebelsList = $('.rebelsList');
                }
                return this._rebelsList;
            },
            _rebeldName: null,
            rebeldName: function () {
                if (this._rebeldName == null) {
                    this._rebeldName = $('.rebeldName');
                }
                return this._rebeldName;
            },
            _rebeldPlanet: null,
            rebeldPlanet: function () {
                if (this._rebeldPlanet == null) {
                    this._rebeldPlanet = $('.rebeldPlanet');
                }
                return this._rebeldPlanet;
            },
            _rebeldRegister: null,
            rebeldRegister: function () {
                if (this._rebeldRegister == null) {
                    this._rebeldRegister = $('.rebeldRegister');
                }
                return this._rebeldRegister;
            }
        };

        var registerRebeld = function (newRebeld) {
            debugger;
            $.post("/api/rebels", newRebeld).done(that.refreshRebeldList);
        };

        this.refreshRebeldList = function () {
            $.get("/api/rebels").done(generateRebeldList);
        };

        var generateRebeldList = function (data) {
            var renderHtml = "<table>"
            for (var i = 0; i < data.length; i++) {
                var rebeld = data[i];
                renderHtml += "<tr><td>" + JSON.stringify(rebeld) + "</td></tr>";
            }
            htmlElemnts.rebelsList().html(renderHtml + "</table>")
        };

        this.bindClickEvent = function () {
            htmlElemnts.rebeldRegister().bind("click", function (event) {
                var newRebeld = {
                    'name': htmlElemnts.rebeldName().val(),
                    'planet': htmlElemnts.rebeldPlanet().val()
                }
                registerRebeld(newRebeld);
            });
        };

    };
    return new AuxClass();
}());

//This autocalled fucntion will always be called on startup
(function (window, document, undefined) {
    window.onload = init;

    function init() {
        ImperialRebels.bindClickEvent();
        ImperialRebels.refreshRebeldList();
    }

})(window, document, undefined);