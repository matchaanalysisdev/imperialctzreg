﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Imperial.Citizen.Registration.Models
{
    public class CitizenModel
    {
        public string Name { get; set; }
        public string Specie { get; set; }
        public string RoleName { get; set; }
        public string ApprovedStatus { get; set; }
        public int Id { get; set; }
    }
}