﻿<%@ Page Title="Rebels" Language="C#" MasterPageFile="~/Site.Master" CodeBehind="Rebels.aspx.cs" Inherits="Imperial.Citizen.Registration.Rebels" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-8">
            <h1>Rebels Found in the Galaxy Usign API / AJAX</h1>
            <div class="rebelsList">

            </div>
        </div>
        <div class="col-md-4">
            <h2>Register API / AJAX</h2>
            <table>
                <tr>
                    <td>
                        Name:
                    </td>
                    <td>
                        <input type="text" class="rebeldName" />
                    </td>                    
                </tr>
                <tr>
                    <td>
                        Planet:
                    </td>
                    <td>
                        <input type="text" class="rebeldPlanet" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="rebeldRegister">Register Rebeld using API</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript" src="Scripts/rebels.js"></script>
</asp:Content>
