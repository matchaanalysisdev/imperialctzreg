﻿using Imperial.Citizen.Registration.Data;
using Imperial.Citizen.Registration.Logs;
using Imperial.Citizen.Registration.Models;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Imperial.Citizen.Registration
{
    public partial class _Default : Page
    {
        [Inject]
        public ICitizenRepository repo { get; set; }

        private void BindCitizenList()
        {
            try
            {
                var citizens = repo.GetCitizens().OrderByDescending(c => c.Created).ToList();
                List<CitizenModel> listCitizens = new List<CitizenModel>();
                foreach (var citizen in citizens)
                {
                    var citizenModel = new CitizenModel()
                    {
                        Name = citizen.Name,
                        RoleName = repo.GetRoleById(citizen.RoleId).FirstOrDefault().RoleName,
                        Specie = citizen.Specie,
                        ApprovedStatus = citizen.ApprovedStatus.ToString(),
                        Id = citizen.Id
                    };
                    listCitizens.Add(citizenModel);
                }
                this.TatooineCitizens.DataSource = listCitizens;
                TatooineCitizens.DataBind();
            }
            catch (Exception ex)
            {
                Logging.WriteLog("Bind Citizen List", ex.Message);
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {           
            if (!IsPostBack)
            {
                BindCitizenList();

                //Bind Role DropDown
                var roles = repo.GetRoles().OrderByDescending(r => r.Created).ToList();
                this.RoleList.DataSource = roles;
                this.RoleList.DataValueField = "Id";
                this.RoleList.DataTextField = "RoleName";
                this.RoleList.DataBind();
                //End Bind Roles

                BindCitizensInRole();
            }
        }

        protected void RoleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindCitizensInRole();
        }

        private void BindCitizensInRole()
        {
            try
            {
                var selectedId = int.Parse(this.RoleList.SelectedValue);
                var citizensInRole = repo.GetCitizens().Where(c => c.RoleId == selectedId)
                .OrderByDescending(c => c.Created)
                .Select(c => c.Name).ToList();
                this.CitizenInRole.DataSource = citizensInRole;
                this.CitizenInRole.DataBind();
                if (citizensInRole.Any())
                {
                    this.NoCitizens.Visible = false;
                }
                else
                {
                    this.NoCitizens.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Logging.WriteLog("Bind Citizen in Rol List", ex.Message);
            }
        }

        protected void TatooineCitizens_RowEditing(object sender, GridViewEditEventArgs e)
        {
            Response.Redirect(string.Format("RegisterCitizen.aspx?id={0}", this.TatooineCitizens.Rows[e.NewEditIndex].Cells[1].Text));
        }
    }
}