﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Imperial.Citizen.Registration.Logs
{
    public static class Logging
    {
        public static void WriteLog(string pyramid, string message)
        {
            try
            {
                var basePath = HostingEnvironment.MapPath("/");
                using (StreamWriter outputFile = new StreamWriter(string.Format("{0}/Logs/ApplicationErrors.txt", basePath), true))
                {
                    outputFile.WriteLine(string.Format("{0} - An error ocurred at Imperial Citizen Registration: {1}, Error Message: {2}",
                        DateTime.Now.ToString(), pyramid, message));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(string.Format("Logger Failed Message: {0}", ex.Message));
            }
        }
    }
}