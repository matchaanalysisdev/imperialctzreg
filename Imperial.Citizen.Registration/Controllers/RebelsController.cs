﻿using Imperial.Citizen.Registration.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Imperial.Citizen.Registration.Controllers
{
    public class RebelsController : ApiController
    {
        private ICitizenRepository _repo;

        public RebelsController(ICitizenRepository repo)
        {
            _repo = repo;
        }

        public IEnumerable<Data.Rebeld> Get()
        {
            return _repo.GetRebels().OrderByDescending(r => r.Registered);
        }

        public HttpResponseMessage Post([FromBody]Rebeld newRebeld)
        {
            if(newRebeld.Registered == default(DateTime))
            {
                newRebeld.Registered = DateTime.Now;
            }
            if(_repo.AddRebeld(newRebeld) && _repo.Save())
            {
                return Request.CreateResponse(HttpStatusCode.Created, newRebeld);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
    }
}
