﻿<%@ Page Title="Register Citizen" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RegisterCitizen.aspx.cs" Inherits="Imperial.Citizen.Registration.RegisterCitizen" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h1>
        <asp:Label ID="pageTitle" runat="server" Text="Register a new citizen"></asp:Label>
    </h1>

    <table>
        <tr>
            <td>Citizen Name:
            </td>
            <td>
                <asp:TextBox ID="Name" runat="server" CssClass="citizenForm textInput"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Specie:
            </td>
            <td>
                <asp:TextBox ID="Specie" runat="server" CssClass="citizenForm textInput"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Role:
            </td>
            <td>
                <asp:DropDownList ID="RoleList" runat="server" AutoPostBack="true" CssClass="citizenForm"></asp:DropDownList>
            </td>
            <td>
                <a href="Roles.aspx" class="citizenForm">Manage Roles</a>
            </td>
        </tr>
        <tr>
            <td>Approved Statatus:
            </td>
            <td>
                <asp:RadioButtonList ID="ApprovedStatusOptions" runat="server" AutoPostBack="true" CssClass="citizenForm" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td>
                <asp:Button ID="Add" runat="server" Text="Register Citizen" CssClass="citizenForm" OnClick="Add_Click" />
                <asp:Label ID="SaveError" runat="server" Text="An Error ocurred in the save process. Contact Support" Visible="False" BorderColor="Red" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>

    <script type="text/javascript" src="Scripts/registration.js"></script>
</asp:Content>
