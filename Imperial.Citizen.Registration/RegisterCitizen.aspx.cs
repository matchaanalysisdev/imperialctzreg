﻿using Imperial.Citizen.Registration.Data;
using Imperial.Citizen.Registration.Logs;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Imperial.Citizen.Registration
{
    public partial class RegisterCitizen : System.Web.UI.Page
    {
        [Inject]
        public ICitizenRepository repo { get; set; }

        private Data.Citizen _citizen;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    //Bind Role DropDown
                    var roles = repo.GetRoles().OrderByDescending(r => r.Created).ToList();
                    this.RoleList.DataSource = roles;
                    this.RoleList.DataValueField = "Id";
                    this.RoleList.DataTextField = "RoleName";
                    this.RoleList.DataBind();
                    //End Bind Roles

                    //Bind Approved Staus
                    Type type = typeof(ApprovedStatusType);
                    var listApprovedStatus = Enum.GetValues(type).Cast<int>().ToDictionary(a => a, a => Enum.GetName(type, a));
                    this.ApprovedStatusOptions.DataSource = listApprovedStatus;
                    this.ApprovedStatusOptions.DataTextField = "Value";
                    this.ApprovedStatusOptions.DataValueField = "Key";
                    this.ApprovedStatusOptions.DataBind();
                    this.ApprovedStatusOptions.SelectedValue = "0";                  
                    //End Bind Approved Status

                    if (Request.QueryString["id"] != null)
                    {
                        _citizen = repo.GetCitizenById(int.Parse(Request.QueryString["id"]));
                        if (_citizen != null)
                        {
                            this.pageTitle.Text = string.Format("Edit Citizen {0}", _citizen.Name);
                            this.Add.Text = "Save Changes";
                            this.RoleList.SelectedValue = _citizen.RoleId.ToString();
                            this.ApprovedStatusOptions.SelectedValue = ((int)_citizen.ApprovedStatus).ToString();
                            this.Name.Text = _citizen.Name;
                            this.Specie.Text = _citizen.Specie;
                        }
                        else
                        {
                            throw new System.Exception("Invalid Citizen Id");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logging.WriteLog("Add Citizen Form", ex.Message);
                }
            }
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["id"] != null)
                {
                    _citizen = repo.GetCitizenById(int.Parse(Request.QueryString["id"]));
                    if (_citizen != null)
                    {
                        _citizen.Name = this.Name.Text;
                        _citizen.Specie = this.Specie.Text;
                        _citizen.RoleId = int.Parse(this.RoleList.SelectedValue);
                        _citizen.ApprovedStatus = (ApprovedStatusType)int.Parse(this.ApprovedStatusOptions.SelectedValue);
                        repo.Save();
                    }
                    else
                    {
                        throw new System.Exception("Invalid Citizen Id");
                    }
                }
                else
                {
                    Data.Citizen newCitizen = new Data.Citizen()
                    {
                        Name = this.Name.Text,
                        Specie = this.Specie.Text,
                        RoleId = int.Parse(this.RoleList.SelectedValue),
                        ApprovedStatus = (ApprovedStatusType)int.Parse(this.ApprovedStatusOptions.SelectedValue),
                        Created = DateTime.Now
                    };
                    if (repo.AddCitizen(newCitizen))
                    {
                        repo.Save();
                    }
                }
                Response.Redirect("/");
            }
            catch (Exception ex)
            {
                this.SaveError.Visible = true;
                Logging.WriteLog("Add Citizen Save", ex.Message);
            }
        }
    }
}