﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Imperial.Citizen.Registration.Data
{
    public class Rebeld
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Planet { get; set; }
        public DateTime Registered { get; set; }
    }
}