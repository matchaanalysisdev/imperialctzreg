﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;


namespace Imperial.Citizen.Registration.Data
{
    public class ImperialCitizenContext : DbContext
    {
        public ImperialCitizenContext()
            : base("CitizenContextConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;

            Database.SetInitializer(
                new MigrateDatabaseToLatestVersion<ImperialCitizenContext, ImperialCitizenMigrationConfiguration>()
                );
        }

        public DbSet<Citizen> Citizens { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Rebeld> Rebels { get; set; }
    }
}