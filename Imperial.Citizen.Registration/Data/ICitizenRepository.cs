﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imperial.Citizen.Registration.Data
{
    public interface ICitizenRepository
    {
        IQueryable<Citizen> GetCitizens();
        IQueryable<Role> GetRoles();
        IQueryable<Role> GetRoleById(int roleId);
        bool AddCitizen(Citizen newCitizen);
        bool AddRole(Role newRole);
        Citizen GetCitizenById(int citizenId);
        IQueryable<Rebeld> GetRebels();
        bool Save();
        bool AddRebeld(Rebeld newRebeld);
    }
}
