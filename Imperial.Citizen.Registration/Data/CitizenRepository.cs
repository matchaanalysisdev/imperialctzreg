﻿using Imperial.Citizen.Registration.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Imperial.Citizen.Registration.Data
{
    public class CitizenRepository : ICitizenRepository
    {
        ImperialCitizenContext _ctx;
        public CitizenRepository(ImperialCitizenContext ctx)
        {
            _ctx = ctx;
        }

        public bool AddCitizen(Citizen newCitizen)
        {
            try
            {
                _ctx.Citizens.Add(newCitizen);
                return true;
            }
            catch (Exception ex)
            {
                Logging.WriteLog("Add New Citizen", ex.Message);
                return false;
            }
        }

        public bool AddRebeld(Rebeld newRebeld)
        {
            try
            {
                _ctx.Rebels.Add(newRebeld);
                return true;
            }
            catch (Exception ex)
            {
                Logging.WriteLog("Add New Rebeld", ex.Message);
                return false;
            }
        }

        public bool AddRole(Role newRole)
        {
            try
            {
                _ctx.Roles.Add(newRole);
                return true;
            }
            catch (Exception ex)
            {
                Logging.WriteLog("Add New Role", ex.Message);
                return false;
            }
        }

        public Citizen GetCitizenById(int citizenId)
        {
            return _ctx.Citizens.Where(c => c.Id == citizenId).FirstOrDefault();
        }

        public IQueryable<Citizen> GetCitizens()
        {
            return _ctx.Citizens;
        }

        public IQueryable<Rebeld> GetRebels()
        {
            return _ctx.Rebels;
        }

        public IQueryable<Role> GetRoleById(int roleId)
        {
            return _ctx.Roles.Where(r => r.Id == roleId);
        }

        public IQueryable<Role> GetRoles()
        {
            return _ctx.Roles;
        }

        public bool Save()
        {
            try
            {
                return _ctx.SaveChanges() > 0;
            }
            catch (Exception ex)
            {
                Logging.WriteLog("Error in data save", ex.Message);
                return false;
            }
        }
    }
}