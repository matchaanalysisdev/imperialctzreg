﻿using Imperial.Citizen.Registration.Logs;
using System;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Imperial.Citizen.Registration.Data
{
    public class ImperialCitizenMigrationConfiguration
        : DbMigrationsConfiguration<ImperialCitizenContext>
    {
        public ImperialCitizenMigrationConfiguration()
        {
            this.AutomaticMigrationDataLossAllowed = true;
            this.AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ImperialCitizenContext context)
        {
            base.Seed(context);
            if (context.Roles.Count() == 0)
            {
                var roles = new string[] { "Queuen", "Prime Minister", "Politician", "Citizien", "Slave" };
                foreach (var roleName in roles)
                {
                    var role = new Role()
                    {
                        RoleName = roleName,
                        Created = DateTime.Now
                    };

                    context.Roles.Add(role);
                }

                try
                {
                    context.SaveChanges();
                }
                catch (System.Exception ex)
                {
                    Logging.WriteLog("Seed DB roles", ex.Message);
                }
            }

            if (context.Citizens.Count() == 0)
            {
                var citizen = new Citizen()
                {
                    Name = "Rafael Gudino",
                    RoleId = context.Roles.FirstOrDefault().Id,
                    Specie = "Human",
                    Created = DateTime.Now,
                    ApprovedStatus = ApprovedStatusType.Unassigned
                };

                context.Citizens.Add(citizen);

                try
                {
                    context.SaveChanges();
                }
                catch (System.Exception ex)
                {
                    Logging.WriteLog("Seed DB Citizens", ex.Message);
                }
            }

            if (context.Rebels.Count() == 0)
            {
                var rebeld = new Rebeld()
                {
                    Name = "Luke Trotacielos",
                    Planet = "Pluto",
                    Registered = DateTime.Now
                };

                context.Rebels.Add(rebeld);

                try
                {
                    context.SaveChanges();
                }
                catch (System.Exception ex)
                {
                    Logging.WriteLog("Seed DB Rebels", ex.Message);
                }
            }
        }
    }
}