﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Imperial.Citizen.Registration.Data
{
    public class Citizen
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Specie { get; set; }
        public ApprovedStatusType ApprovedStatus { get; set; }
        public DateTime Created { get; set; }
        public int RoleId { get; set; }
    }

    public enum ApprovedStatusType
    {
        Unassigned = 0,
        ImperiumCitizen = 1,
        Rebeld = 2
    }
}