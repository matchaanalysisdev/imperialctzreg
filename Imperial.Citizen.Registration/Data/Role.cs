﻿using System;

namespace Imperial.Citizen.Registration.Data
{
    public class Role
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
        public DateTime Created { get; set; }
    }
}