﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Imperial.Citizen.Registration.Data;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Imperial.Citizen.Registration.Controllers;
using System.Web.Http.Hosting;
using System.Web.Http;
using System.Net.Http;

namespace Imperial.Citizen.Registration.Tests
{
    [TestClass]
    public class ApiTests
    {
        private Mock<ICitizenRepository> _mockCitizenRepository;
        private Data.Rebeld _rebel;

        [TestInitialize]
        public void Init()
        {
            _rebel = new Data.Rebeld
            {
                Id = 1,
                Name = "Test",
                Planet = "Test",
                Registered = DateTime.Now
            };

            IQueryable<Data.Rebeld> listRebels = new List<Data.Rebeld>() { _rebel }.AsQueryable();
            _mockCitizenRepository = new Mock<ICitizenRepository>();
            _mockCitizenRepository.Setup(o => o.GetRebels()).Returns(listRebels);
            _mockCitizenRepository.Setup(o => o.AddRebeld(It.IsAny<Rebeld>())).Returns(false);
            _mockCitizenRepository.Setup(o => o.AddRebeld(_rebel)).Returns(true);
            _mockCitizenRepository.Setup(o => o.Save()).Returns(true);
        }

        [TestMethod]
        public void test_RebelController_GetRebels_OK()
        {
            //Arrange
            var controller = new RebelsController(_mockCitizenRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            //Act
            var test = controller.Get();
            //Assert
            Assert.IsTrue(test.Any());
        }

        [TestMethod]
        public void test_RebelController_PostRebels_Fails()
        {
            //Arrange
            var controller = new RebelsController(_mockCitizenRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            //Act
            var testRebeld = new Rebeld()
            {
                Name = "badTest",
                Planet = "badPlanet"
            };
            var test = controller.Post(testRebeld);
            //Assert
            Assert.IsTrue(test.StatusCode == System.Net.HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public void test_RebelController_PostRebels_Ok_Save()
        {
            //Arrange
            var controller = new RebelsController(_mockCitizenRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            //Act
            var test = controller.Post(_rebel);
            //Assert
            Assert.IsTrue(test.StatusCode == System.Net.HttpStatusCode.Created);
        }
    }
}
